# Sample E-Commerce Website 



- ### Objectif du projet

  - #### Présentation général du projet

    - Kyzz est un site de e-Commerce spécialiser dans la vente de prêt à porter.

------

- ### Identité graphique

  - #### Impression générale.

    - Très épuré avec des couleurs simples (gris/blanc/noir/bleu/rose saumon) 

    

  - #### Inspirations.

    - https://colorlib.com/preview/#scenic

------

- ### Description du site internet

  - #### Principales pages.

    - Accueil (Présentation du site, quelques produits en avant configuré via le back office).
    - Catégories Produit (Pouvant être configuré sur le back office).
    - Espace Client.
      - Modification des informations perso.
      - Suivi de commandes.
      - Support / SAV.

    

  - #### Principales fonctionnalités.

    - User 

      - Ajouter un commentaire sur un produit.
      - Espace client cité ci-dessus.

    - Admin

      - Gestion des produits.
        - Ajout / suppression / modification
        - Pouvoir mettre un produit en avant
        - Description / prix / photos du produit
      - Gestion des catégories produit.
      - Gestion des utilisateurs.
      - Gestion des commandes.
      - Gestion de stocks (à voir et non prioritaire)

      

  - #### Technologies utilisés.

    - PHP, SQL, HTML, CSS, JS
      - Utilisation du framework backend Phalcon.
        - Utilisation de l'ORM Phalcon.
        - Utilisation du moteur de template volt.