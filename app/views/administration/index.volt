<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-tags"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Nombre de produits</span>
                <span class="info-box-number">00</span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-book"></i></span>

            <div class="info-box-content">
                <span class="info-box-text">Nombre de catégories</span>
                <span class="info-box-number">00</span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">Derniers produits ajouté</h3>
            </div>
            <div class="box-body">
                <ul class="products-list product-list-in-box">
                    
                    {% for lastproduct in lastproduct %}
                    <li class="item">
                        <div class="product-img">
                            <img src="{{ lastproduct.img_url }}" alt="Product Image">
                        </div>
                        <div class="product-info">
                            <a href="/shop/product?id={{ lastproduct.id }}" target="_blank" class="product-title">{{ lastproduct.name }}
                                <span class="label label-success pull-right">{{ lastproduct.price - lastproduct.reduction }} €</span></a>
                            <span class="product-description">
                                {{ lastproduct.description }}
                            </span>
                        </div>
                    </li>
                    {% endfor %}


                </ul>
            </div>

            <div class="box-footer text-center">
                <a href="{{ url('/administration/product') }}" class="uppercase">Voir plus</a>
            </div>

        </div>
    </div>
</div>
</section>
</div>
</div>

</div>

