<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border"></div>
        <div class="box-body">
            <div class="col-md-12">
                <form action="#" name="addproduct" method="POST">

                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-mouse-pointer"></i></span>
                        <input type="text" name="name" class="form-control" placeholder="Nom du produit">
                    </div>
                    <br>

                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-edit"></i></span>
                        <textarea type="text" name="description" class="form-control"
                            placeholder="Description"></textarea>
                    </div>
                    <br>

                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-list-alt"></i></span>
                        <select name="category" class="form-control">
                            <option value=""> -- Catégorie du produit -- </option>
                            {% for allcategory in allcategory %}
                            {%- if allcategory.active == 1 -%}
                            <option value="{{ allcategory.id }}">{{ allcategory.name }}</option>
                            {%- endif -%}
                            {% endfor %}
                        </select>
                    </div>
                    <br>

                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-euro"></i></span>
                        <input type="number" name="price" class="form-control" placeholder="Prix unitaire">
                    </div>
                    <br>

                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-arrow-circle-down"></i></span>
                        <input type="number" name="reduction" class="form-control" placeholder="Réductions">
                    </div>
                    <br>

                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-arrows-h"></i></span>
                        <input type="text" name="size" class="form-control"
                            placeholder="Tailles disponible séparé par une virgule">
                    </div>
                    <br>

                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-file-photo-o"></i></span>
                        <input type="text" name="img" class="form-control" placeholder="URL de la photo du produit">
                    </div>
                    <br>

                    <button type="submit" name="submit" class="btn btn-success btn-xs btn-block">Valider</button>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="col-md-8">
    <div class="box box-primary">
        <div class="box-header with-border"></div>
        <div class="box-body">
            <div class="col-md-12">
                <form action="#" name="modify" method="POST">


                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>#</th>
                          <th>Nom du produit</th>
                          <th>Prix (sans réduc)</th>
                          <th>Prix (avec réduc)</th>
                          <th>Image (url)</th>
                          <th>Date d'ajout</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                            {% for allproduct in allproduct %}
                            <tr>
                                <td>{{ allproduct.id }}</td>
                                <td>{{ allproduct.name }}</td>
                                <td>{{ allproduct.price }}€</td>
                                <td>{{ allproduct.price - allproduct.reduction }}€</td>
                                <td><a href="{{ allproduct.img_url }}" target="_blank">IMAGE</a></td>
                                <td>{{ allproduct.added_time }}</td>
                                <td><a href="/administration/product?delete={{ allproduct.id }}" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></a></td>
                              </tr>
                            {% endfor %}
                        
                        </tbody>
                      </table>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
