<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border"></div>
        <div class="box-body">
            <div class="col-md-12">
                <form action="#" name="config" method="POST">

                    <strong>Nom du site web</strong>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-odnoklassniki"></i></span>
                        <input type="text" name="name" class="form-control" placeholder="Nom du site web" value="{{ configglobal.website_name }}">
                    </div>
                    <br>

                    <strong>Description (courte)</strong>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-tag"></i></span>
                        <input type="text" name="desc" class="form-control" placeholder="Description (courte)" value="{{ configglobal.website_desc }}">
                    </div>
                    <br>

                    <strong>URL</strong>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-underline"></i></span>
                        <input type="text" name="url" class="form-control" placeholder="URL" value="{{ configglobal.website_url }}">
                    </div>
                    <br>

                    <strong>Webmaster E-Mail</strong>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-mail-forward"></i></span>
                        <input type="text" name="mail" class="form-control" placeholder="Webmaster E-Mail" value="{{ configglobal.website_webmaster }}">
                    </div>
                    <br>

                    <strong>Numéro de téléphone</strong>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-phone"></i></span>
                        <input type="text" name="phone" class="form-control" placeholder="Numéro de téléphone" value="{{ configglobal.website_phone }}">
                    </div>
                    <br>

                    <strong>Adressse postal</strong>
                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-map-pin"></i></span>
                        <input type="text" name="location" class="form-control" placeholder="Adressse postal" value="{{ configglobal.website_adress }}">
                    </div>
                    <br>



                    <button type="submit" name="submit" class="btn btn-success btn-xs btn-block">Valider</button>

                </form>
            </div>
        </div>
    </div>
</div>
</div>