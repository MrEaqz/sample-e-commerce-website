<div class="col-md-4">
    <div class="box box-primary">
        <div class="box-header with-border"></div>
        <div class="box-body">
            <div class="col-md-12">
                <form action="#" name="addcat" method="POST">

                    <div class="input-group">
                        <span class="input-group-addon"><i class="fa fa-mouse-pointer"></i></span>
                        <input type="text" name="name" class="form-control" placeholder="Nom de la catégorie">
                    </div>
                    <br>

                    <button type="submit" name="submit" class="btn btn-success btn-xs btn-block">Valider</button>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="col-md-8">
    <div class="box box-primary">
        <div class="box-header with-border"></div>
        <div class="box-body">
            <div class="col-md-12">
                <form action="#" name="modify" method="POST">


                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                          <th>#</th>
                          <th>Nom de la catégorie</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                            {% for allcategory in allcategory %}
                            <tr>
                                <td>{{ allcategory.id }}</td>
                                <td>{{ allcategory.name }}</td>
                                <td>
                                    {%- if allcategory.active == 1 -%}
                                    Actif
                                    {%- else -%}
                                    Inactif
                                    {%- endif -%}
                                </td>
                                <td>
                                    <a href="/administration/category?active={{ allcategory.id }}" class="btn btn-success btn-xs"><i class="fa fa-fw fa-check"></i></a>
                                    <a href="/administration/category?desactiv={{ allcategory.id }}" class="btn btn-warning btn-xs"><i class="fa fa-fw fa-minus-circle"></i></a>
                                    <a href="/administration/category?delete={{ allcategory.id }}" class="btn btn-danger btn-xs"><i class="fa fa-fw fa-trash"></i></a>
                                </td>
                              </tr>
                            {% endfor %}
                        
                        </tbody>
                      </table>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
