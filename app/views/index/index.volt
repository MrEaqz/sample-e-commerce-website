<div class="hero-wrap js-fullheight" style="background-image: url('{{ banner }}');">
	<div class="overlay"></div>
	<div class="container">
	  <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center">
		<h3 class="v"> {{ configglobal.website_name }} - {{ configglobal.website_desc }} </h3>
		<h3 class="vr">Since - 1985</h3>
		<div class="col-md-11 ftco-animate text-center">
		  <h1>{{ configcustom.principal_text }}</h1>
		  <h2><span>{{ configcustom.secondary_text }}</span></h2>
		</div>
		<div class="mouse">
		  <a href="#" class="mouse-icon">
			<div class="mouse-wheel"><span class="ion-ios-arrow-down"></span></div>
		  </a>
		</div>
	  </div>
	</div>
  </div>

  
  
  
  
  <section class="ftco-section ftco-section-more img" style="background-image: url(images/bg_5.jpg);">
	<div class="container">
		<div class="row justify-content-center mb-3 pb-3">
			<div class="col-md-12 heading-section ftco-animate">
				<h2>Soldes d'été</h2>
			</div>
        </div>
	</div>
</section>

<section class="ftco-section testimony-section bg-light">
	<div class="container">
		<div class="row justify-content-center mb-3 pb-3">
			<div class="col-md-12 heading-section text-center ftco-animate">
				<h1 class="big">Testimony</h1>
			</div>
        </div>    		
        <div class="row justify-content-center">
			<div class="col-md-8 ftco-animate">
				<div class="row ftco-animate">
					<div class="col-md-12">
						<div class="carousel-testimony owl-carousel ftco-owl">
							<div class="item">
								<div class="testimony-wrap py-4 pb-5">
									<div class="user-img mb-4" style="background-image: url(images/person_1.jpg)">
										<span class="quote d-flex align-items-center justify-content-center">
											<i class="icon-quote-left"></i>
										</span>
									</div>
									<div class="text text-center">
										<p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
										<p class="name">Roger Scott</p>
										<span class="position">Customer</span>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="testimony-wrap py-4 pb-5">
									<div class="user-img mb-4" style="background-image: url(images/person_2.jpg)">
										<span class="quote d-flex align-items-center justify-content-center">
											<i class="icon-quote-left"></i>
										</span>
									</div>
									<div class="text text-center">
										<p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
										<p class="name">Roger Scott</p>
										<span class="position">Customer</span>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="testimony-wrap py-4 pb-5">
		                  <div class="user-img mb-4" style="background-image: url(images/person_3.jpg)">
		                    <span class="quote d-flex align-items-center justify-content-center">
								<i class="icon-quote-left"></i>
		                    </span>
						</div>
						<div class="text text-center">
							<p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
		                    <p class="name">Roger Scott</p>
		                    <span class="position">Customer</span>
						</div>
					</div>
		              </div>
		              <div class="item">
						  <div class="testimony-wrap py-4 pb-5">
							  <div class="user-img mb-4" style="background-image: url(images/person_1.jpg)">
								<span class="quote d-flex align-items-center justify-content-center">
									<i class="icon-quote-left"></i>
								</span>
							</div>
							<div class="text text-center">
								<p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
								<p class="name">Roger Scott</p>
								<span class="position">Customer</span>
							</div>
		                </div>
					</div>
					<div class="item">
						<div class="testimony-wrap py-4 pb-5">
							<div class="user-img mb-4" style="background-image: url(images/person_1.jpg)">
								<span class="quote d-flex align-items-center justify-content-center">
									<i class="icon-quote-left"></i>
								</span>
							</div>
							<div class="text text-center">
								<p class="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. It is a paradisematic country, in which roasted parts of sentences fly into your mouth.</p>
								<p class="name">Roger Scott</p>
								<span class="position">Customer</span>
							</div>
		                </div>
					</div>
				</div>
		          </div>
		        </div>
			</div>
        </div>
	</div>
</section>

<section class="ftco-section bg-light ftco-services">
	<div class="container">
		<div class="row justify-content-center mb-3 pb-3">
			<div class="col-md-12 heading-section text-center ftco-animate">
				<h1 class="big">Services</h1>
				<h2>We want you to express yourself</h2>
			</div>
        </div>
        <div class="row">
			<div class="col-md-4 text-center d-flex align-self-stretch ftco-animate">
				<div class="media block-6 services">
					<div class="icon d-flex justify-content-center align-items-center mb-4">
						<span class="flaticon-002-recommended"></span>
					</div>
					<div class="media-body">
						<h3 class="heading">Satisfait ou remboursé</h3>
						<p>Vous avez 14 jours pour nous retourner le produit.</p>
					</div>
				</div>      
			</div>
			<div class="col-md-4 text-center d-flex align-self-stretch ftco-animate">
				<div class="media block-6 services">
					<div class="icon d-flex justify-content-center align-items-center mb-4">
						<span class="flaticon-001-box"></span>
					</div>
					<div class="media-body">
						<h3 class="heading">Livraison rapide</h3>
						<p>Livraison sous 48h ouvré en france métropolitaine.</p>
					</div>
				</div>    
			</div>
			<div class="col-md-4 text-center d-flex align-self-stretch ftco-animate">
				<div class="media block-6 services">
					<div class="icon d-flex justify-content-center align-items-center mb-4">
						<span class="flaticon-003-medal"></span>
					</div>
					<div class="media-body">
						<h3 class="heading">Qualité supérieur</h3>
						<p>Nos sous-vêtements sont confectionner dans un matériau haut de gamme.</p>
					</div>
				</div>      
          </div>
        </div>
	</div>
</section>
<div class="goto-here"></div>

