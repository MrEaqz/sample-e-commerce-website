<head>
  {{ get_title() }}
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- <link href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700,900" rel="stylesheet"> -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet"> -->

  <link rel="stylesheet" href="{{ url('css/open-iconic-bootstrap.min.css') }}">
  <link rel="stylesheet" href="{{ url('css/animate.css') }}">

  <link rel="stylesheet" href="{{ url('css/owl.carousel.min.css') }}">
  <link rel="stylesheet" href="{{ url('css/owl.theme.default.min.css') }}">
  <link rel="stylesheet" href="{{ url('css/magnific-popup.css') }}">

  <link rel="stylesheet" href="{{ url('css/aos.css') }}">

  <link rel="stylesheet" href="{{ url('css/ionicons.min.css') }}">

  <link rel="stylesheet" href="{{ url('css/bootstrap-datepicker.css') }}">
  <link rel="stylesheet" href="{{ url('css/jquery.timepicker.css') }}">


  <link rel="stylesheet" href="{{ url('css/flaticon.css') }}">
  <link rel="stylesheet" href="{{ url('css/icomoon.css') }}">
  <link rel="stylesheet" href="{{ url('css/style.css') }}">
</head>

<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
  <div class="container">
    <a class="navbar-brand" href="{{ url('index') }}">{{ configglobal.website_name }}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav"
      aria-expanded="false" aria-label="Toggle navigation">
      <span class="oi oi-menu"></span> Menu
    </button>

    <div class="collapse navbar-collapse" id="ftco-nav">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item active"><a href="{{ url('index') }}" class="nav-link">Accueil</a></li>
            {% for allcategory in allcategory %}
            {%- if allcategory.active == 1 -%}
            <li class="nav-item"><a href="/shop/category?number={{ allcategory.id }}" class="nav-link">{{ allcategory.name }}</a></li>

            {%- endif -%}
            {% endfor %}
        <li class="nav-item cta cta-colored"><a href="cart.html" class="nav-link"><span
              class="icon-shopping_cart"></span>[0]</a></li>

      </ul>
    </div>
  </div>
</nav>
<!-- END nav -->

{{ content() }}


<section class="ftco-section-parallax">
  <div class="parallax-img d-flex align-items-center">
    <div class="container">
      <div class="row d-flex justify-content-center py-5">
        <div class="col-md-7 text-center heading-section ftco-animate">
          <h1 class="big">Newletter</h1>
          <h2>S'abonner à notre newletter</h2>
          <div class="row d-flex justify-content-center mt-5">
            <div class="col-md-8">
              <form action="#" class="subscribe-form">
                <div class="form-group d-flex">
                  <input type="text" class="form-control" placeholder="Votre adresse mail ici">
                  <input type="submit" value="S'abonner" class="submit px-3">
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<footer class="ftco-footer bg-light ftco-section">
    <div class="row">
      <div class="col-md-12 text-center">

        <p>
          Copyright &copy;
          <script>document.write(new Date().getFullYear());</script> Tous droits réservés | Site web fait avec le  <i
            class="icon-heart color-danger" aria-hidden="true"></i> par <a href="#"
            target="_blank">Kylian Zaïmi</a>        
        </p>

      </div>
    </div>
  </dsiv>
</footer>



<!-- loader -->
<div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
    <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
    <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10" stroke="#F96D00" />
    </svg></div>

<script src="{{ url('js/jquery.min.js') }}"></script>
<script src="{{ url('js/jquery-migrate-3.0.1.min.js') }}"></script>
<script src="{{ url('js/popper.min.js') }}"></script>
<script src="{{ url('js/bootstrap.min.js') }}"></script>
<script src="{{ url('js/jquery.easing.1.3.js') }}"></script>
<script src="{{ url('js/jquery.waypoints.min.js') }}"></script>
<script src="{{ url('js/jquery.stellar.min.js') }}"></script>
<script src="{{ url('js/owl.carousel.min.js') }}"></script>
<script src="{{ url('js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ url('js/aos.js') }}"></script>
<script src="{{ url('js/jquery.animateNumber.min.js') }}"></script>
<script src="{{ url('js/bootstrap-datepicker.js') }}"></script>
<script src="{{ url('js/scrollax.min.js') }}"></script>
<!-- <script src="{{ url('https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false') }}"></script> -->
<script src="{{ url('js/google-map.js') }}"></script>
<script src="{{ url('js/main.js') }}"></script>