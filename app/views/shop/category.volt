<div class="hero-wrap hero-bread" style="background-image: url('../images/bg_6.jpg');">
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-0 bread">Collection</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="{{ url('index') }}">Acceuil</a></span> <span>Produits</span></p>
        </div>
      </div>
    </div>
  </div>
      
      <section class="ftco-section bg-light">
      <div class="container-fluid">
          <div class="row">

              {% for products in products %}

              <div class="col-sm col-md-6 col-lg-3 ftco-animate">
                <div class="product">
                    <a href="/shop/product?id={{ products.id }}" class="img-prod"><img class="img-fluid" src="{{ products.img_url }}" alt="Colorlib Template">
                        <span class="status">-{{ products.reduction }}€</span>
                    </a>
                    <div class="text py-3 px-3">
                        <h3><a href="#">{{ products.name }}</a></h3>
                        <div class="d-flex">
                            <div class="pricing">
                                <p class="price"><span class="mr-2 price-dc">{{ products.price }}€</span><span class="price-sale">{{ products.price - products.reduction }}€</span></p>
                            </div>
                            <div class="rating">
                                <p class="text-right">
                                    <span class="ion-ios-star"></span>
                                    <span class="ion-ios-star"></span>
                                    <span class="ion-ios-star"></span>
                                    <span class="ion-ios-star-half"></span>
                                    <span class="ion-ios-star-outline"></span>
                                </p>
                            </div>
                        </div>
                        <hr>
                        <p class="bottom-area d-flex">
                            <a href="#" class="add-to-cart"><span>Ajouter au panier <i class="ion-ios-add ml-1"></i></span></a>
                        </p>
                    </div>
                </div>
            </div>

              {% endfor %}


          </div>
          <!-- <div class="row mt-5">
        <div class="col text-center">
          <div class="block-27">
            <ul>
              <li><a href="#">&lt;</a></li>
              <li class="active"><span>1</span></li>
              <li><a href="#">2</a></li>
              <li><a href="#">3</a></li>
              <li><a href="#">4</a></li>
              <li><a href="#">5</a></li>
              <li><a href="#">&gt;</a></li>
            </ul>
          </div>
        </div>
      </div> -->
      </div>
  </section>