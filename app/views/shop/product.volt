<div class="hero-wrap hero-bread" style="background-image: url('/images/bg_6.jpg');">
    <div class="container">
      <div class="row no-gutters slider-text align-items-center justify-content-center">
        <div class="col-md-9 ftco-animate text-center">
          <h1 class="mb-0 bread">Product Single</h1>
          <p class="breadcrumbs"><span class="mr-2"><a href="{{ url('index') }}">Accueil</a></span> <span class="mr-2"><a href="{{ url('shop/category?number=1') }}">Produits</a></span> <span>Fiche produit</span></p>
        </div>
      </div>
    </div>
  </div>
      
      <section class="ftco-section bg-light">
      <div class="container">
          <div class="row">
              <div class="col-lg-6 mb-5 ftco-animate">
                  <a href="{{ product.img_url }}" class="image-popup"><img src="{{ product.img_url }}" class="img-fluid"></a>
              </div>
              <div class="col-lg-6 product-details pl-md-5 ftco-animate">
                  <h3>{{ product.name }}</h3>
                  <p class="price"><span>{{ product.price - product.reduction }}€</span></p>
                  <p>{{ product.description }}</p>
                      </p>
                      <div class="row mt-4">
                          <div class="col-md-6">
                              <div class="form-group d-flex">
                    <div class="select-wrap">
                    <div class="icon"><span class="ion-ios-arrow-down"></span></div>
                    <select name="" id="" class="form-control">
                        <option value="">A</option>
                      <option value="">B</option>
                      <option value="">C</option>
                      <option value="">D</option>
                    </select>
                  </div>
                  </div>
                          </div>
                          <div class="w-100"></div>
                          <div class="input-group col-md-6 d-flex mb-3">
                   <span class="input-group-btn mr-2">
                      <button type="button" class="quantity-left-minus btn"  data-type="minus" data-field="">
                     <i class="ion-ios-remove"></i>
                      </button>
                      </span>
                   <input type="text" id="quantity" name="quantity" class="form-control input-number" value="1" min="1" max="100">
                   <span class="input-group-btn ml-2">
                      <button type="button" class="quantity-right-plus btn" data-type="plus" data-field="">
                       <i class="ion-ios-add"></i>
                   </button>
                   </span>
                </div>
            </div>
            <p><a href="cart.html" class="btn btn-primary py-3 px-5">Add to Cart</a></p>
              </div>
          </div>
      </div>
  </section>