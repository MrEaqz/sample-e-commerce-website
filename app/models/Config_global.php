<?php

use Phalcon\Mvc\Model;

class Config_global extends Model
{

    public $id;
    public $website_name;
    public $website_desc;
    public $website_url;
    public $website_webmaster;
    public $website_phone;
    public $website_adress;
    public $maintenance;


    public function initialize()
    {
        $this->useDynamicUpdate(true);
        $this->setSource("config_global");
    }

}
