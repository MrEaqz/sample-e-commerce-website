<?php

use Phalcon\Mvc\Model;

class Config_pages extends Model
{

    public $id;
    public $page_id;
    public $principal_text;
    public $secondary_text;
    public $banner_url;
    

    public function initialize()
    {
        $this->setSource("config_pages");
    }

}
