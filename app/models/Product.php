<?php

use Phalcon\Mvc\Model;

class Product extends Model
{

    public $id;
    public $name;
    public $description;
    public $category_id;
    public $price;
    public $reduction;
    public $size;
    public $img_url;
    public $stock_left;
    public $added_time;
    public $added_by_user_id;

    public function initialize()
    {
        $this->setSource("product");
    }

}
