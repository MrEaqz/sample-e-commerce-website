<?php

class IndexController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Accueil');

        parent::initialize(); 
    }

    public function indexAction()
    {
        $pageNumber = 1;

        $configCustom = Config_pages::Find([
            'conditions' => "page_id = $pageNumber"
        ]);

        $this->view->configcustom = $configCustom[0];
        $this->view->banner       = $configCustom[0]->banner_url;
    }

}


