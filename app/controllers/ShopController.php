<?php
use Phalcon\Mvc\Model\Query;

class ShopController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Accueil');

        parent::initialize(); 
    }

    public function categoryAction()
    {
        $categoryId = $this->request->getQuery("number", "int");
        if (isset($categoryId) && !empty($categoryId)) {
            $product = Product::find([
                'conditions' => "category_id = $categoryId"
            ]);
            $this->view->products = $product;

        } else {
            $this->response->redirect('/errors/show404');
        }
    }

    public function productAction()
    {
        $productId = $this->request->getQuery("id", "int");
        if (isset($productId) && !empty($productId)) {
            $product = Product::find([
                'conditions' => "id = $productId"
            ]);
            $this->view->product = $product[0];

        } else {
            $this->response->redirect('/errors/show404');
        }
    }

}