<?php
declare(strict_types=1);

class ErrorsController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Oops!');
        $this->view->setTemplateAfter('errors');
    }

    public function show404Action(): void
    {
        $this->response->setStatusCode(404);
    }

    public function show401Action(): void
    {
        $this->response->setStatusCode(401);
    }

    public function show500Action(): void
    {
        $this->response->setStatusCode(500);
    }

    public function show503Action(): void
    {
        $this->response->setStatusCode(503);
    }
}

