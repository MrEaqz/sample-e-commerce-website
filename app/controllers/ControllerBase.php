<?php

use Phalcon\Mvc\Controller;
use Phalcon\Filter;
use Phalcon\Http\Response;

class ControllerBase extends Controller
{
    protected function initialize()
    {
        $this->tag->prependTitle('ExB | ');

        $this->view->setTemplateAfter('main');

        $allCategory = Category::Find();
        $this->view->allcategory       = $allCategory;

        $configGlobal = Config_global::Find();
        $this->view->configglobal       = $configGlobal[0];

    }
}
