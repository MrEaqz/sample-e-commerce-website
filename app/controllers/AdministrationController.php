<?php
use Phalcon\Mvc\Model\Query;

class AdministrationController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Oops!');
        $this->view->setTemplateAfter('admin');

        $configGlobal = Config_global::Find();
        $this->view->configglobal       = $configGlobal[0];
    }

    public function indexAction()
    {
        $lastProduct = Product::Find([
            'limit' => 3,
            'order' => 'id DESC'
        ]);
        $this->view->lastproduct       = $lastProduct;
    }

    public function loginAction()
    {
    }

    public function logoutAction()
    {
    }

    public function productAction()
    {
//
        $allCategory = Category::Find();
        $this->view->allcategory       = $allCategory;

        $allProducts = Product::Find();
        $this->view->allproduct       = $allProducts;

        $deleteId = $this->request->getQuery("delete");

        if (isset($deleteId) && !empty($deleteId)) {
            $productDelete = Product::FindFirst("id = $deleteId");
            $productDelete->delete();
            $this->response->redirect("/administration/product");
        }

        if ($this->request->isPost()) {
            $name           = $this->request->getPost('name');
            $description    = $this->request->getPost('description');
            $category       = $this->request->getPost('category');
            $price          = $this->request->getPost('price');
            $reduction      = $this->request->getPost('reduction');
            $size           = $this->request->getPost('size');
            $img            = $this->request->getPost('img');

            if (isset($name) && isset($description) && isset($category) && isset($price) && isset($reduction) && isset($size) && isset($img)) {
                if (!empty($name) && !empty($description) && !empty($category) && !empty($price) && !empty($reduction) && !empty($size) && !empty($img)) {
                    if ($price >= 0) {
                        if ($category == 0) {
                            print_r("Vous devez obligatoirement séléctionner une categorie valide.");
                            die;
                        } else {
                            $product = new Product();
                            $product->name = $name;
                            $product->description = $description;
                            $product->category_id = $category;
                            $product->price = $price;
                            $product->reduction = $reduction;
                            $product->size = $size;
                            $product->img_url = $img;
                            $product->stock_left = 0;
                            $product->added_by_user_id = 0;
                        
                            if ($product->save() === false) {
                                print_r("Hmm, je n'arrive pas à enregistrer le produit pour l'instant : \n");
                                die;
                        
                                $messages = $product->getMessages();
                        
                                foreach ($messages as $message) {
                                    print_r($message, "\n");
                                    die;
                                }
                            } else {
                                print_r('Parfait ! Le produit à été ajouté avec success.');
                                die;
                            }
                        }
                    } else {
                        print_r("Le prix n'est pas correctement remplis (celui-ci doit être supérieur à 0).");
                        die;
                    }
                } else {
                    print_r("Tous les champs ne sont pas correctement remplis.");
                    die;
                }
            }
        }
//
    }

    public function categoryAction()
    {
        $allCategory = Category::Find();
        $this->view->allcategory       = $allCategory;

        $allProducts = Product::Find();
        $this->view->allproduct       = $allProducts;

        $deleteId = $this->request->getQuery("delete");
        if (isset($deleteId) && !empty($deleteId)) {
            $categoryDelete = Category::FindFirst("id = $deleteId");
            $categoryDelete->delete();
            $this->response->redirect("/administration/category");
        }

        $activeId = $this->request->getQuery("active");
        if (isset($activeId) && !empty($activeId)) {
            $categoryActive = Category::FindFirst("id = $activeId");
            $categoryActive->active = 1;
            $categoryActive->update();
            $this->response->redirect("/administration/category");
        }

        $desactivId = $this->request->getQuery("desactiv");
        if (isset($desactivId) && !empty($desactivId)) {
            $categoryDesactiv = Category::FindFirst("id = $desactivId");
            $categoryDesactiv->active = 0;
            $categoryDesactiv->update();
            $this->response->redirect("/administration/category");
        }

        if ($this->request->isPost()) {
            $name           = $this->request->getPost('name');
            if (isset($name)) {
                if (!empty($name)) {
                            $category = new Category();
                            $category->name = $name;
                        
                            if ($category->save() === false) {
                                print_r("Hmm, je n'arrive pas à enregistrer la catégorie pour l'instant : \n");
                                die;
                        
                                $messages = $product->getMessages();
                        
                                foreach ($messages as $message) {
                                    print_r($message, "\n");
                                    die;
                                }
                            } else {
                                print_r('Parfait ! La catégorie à été ajouté avec success.');
                                $this->response->redirect("/administration/category");
                            }
                } else {
                    print_r("Tous les champs ne sont pas correctement remplis.");
                    die;
                }
            }
        }
    }

    public function identityAction()
    {
        if ($this->request->isPost()) {
            $name       = $this->request->getPost('name');
            $desc       = $this->request->getPost('desc');
            $url        = $this->request->getPost('url');
            $mail       = $this->request->getPost('mail');
            $phone      = $this->request->getPost('phone');
            $location   = $this->request->getPost('location');

            if (isset($name) && isset($desc) && isset($url) && isset($mail) && isset($phone) && isset($location)) {
                if (!empty($name) && !empty($desc) && !empty($url) && !empty($mail) && !empty($phone) && !empty($location)) {

                            $configuration = Config_global::FindFirst();
                            $configuration->website_name        = $name;
                            $configuration->website_desc        = $desc;
                            $configuration->website_url         = $url;
                            $configuration->website_webmaster   = $mail;
                            $configuration->website_phone       = $phone;
                            $configuration->website_adress      = $location;
                        
                            if ($configuration->update() === false) {
                                print_r("Erreur : \n"); die;
                        
                                $messages = $configuration->getMessages();
                        
                                foreach ($messages as $message) {
                                    print_r($message, "\n"); die;
                                }
                            } else {
                                $this->response->redirect('/administration/identity');
                            }

                } else {
                    print_r("Tous les champs ne sont pas correctement remplis.");
                    die;
                }
            }
        }
    }

    public function configAction()
    {
        
    }
}
